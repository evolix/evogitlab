#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# bash gitlab_install.sh gitlab-maarch labs.maarch.org equipe@evolix.fr D*******
gitlabUser="$1"
gitlabVersion="v8.16.3"
host="$2"
psqlPass="$(apg -m21 -n1)"
GITLAB_ROOT_PASSWORD="$3"
GITLAB_ROOT_EMAIL="$4"

# TODO: Check args, and creates usage()
# TODO: Splits parts with functions like database(), redis(), gitlabShell(), ...

###cd /tmp
###adduser --disabled-login --gecos "GitLab instance $gitlabUser" $gitlabUser
###echo "CREATE ROLE \"${gitlabUser}\" WITH ENCRYPTED PASSWORD '$psqlPass' CREATEDB LOGIN;" \
###  | sudo -u postgres psql
###sudo -u postgres createdb -O $gitlabUser -E UNICODE $gitlabUser
###
###cat <<EOT > /etc/redis/${gitlabUser}.conf
###daemonize yes
###pidfile /var/run/redis/${gitlabUser}.pid
###port 0
###unixsocket /var/run/redis/${gitlabUser}.sock
###unixsocketperm 770
###timeout 0
###loglevel notice
###logfile /var/log/redis/${gitlabUser}.log
###databases 16
###save 900 1
###save 300 10
###save 60 10000
###stop-writes-on-bgsave-error yes
###rdbcompression yes
###rdbchecksum yes
###dbfilename ${gitlabUser}.rdb
###dir /var/lib/redis
###EOT
###chmod 644 /etc/redis/${gitlabUser}.conf
###systemctl enable redis@${gitlabUser}
###systemctl start redis@${gitlabUser}
#### TODO: umask is only setted with sudo -i with .profile...
#### *BUT* sudo -i set $HOME, and we do not want.
#### *AND* sudo -u do not set umask...
#### *SO* NO UMASK ATM!
###echo "umask 002" >> /home/${gitlabUser}/.profile
###echo "umask 002" >> /home/${gitlabUser}/.bashrc
cd /home/${gitlabUser}
###sudo -u $gitlabUser git clone https://gitlab.com/gitlab-org/gitlab-ce.git -b 8-14-stable gitlab
cd gitlab
###sudo -u $gitlabUser cp config/gitlab.yml.example config/gitlab.yml
###sudo -u $gitlabUser sed -i -e "s@/home/git@/home/${gitlabUser}@g" \
###  -e "s@host: localhost@host: ${host}@" \
###  -e "s@port: 80@port: 443@" \
###  -e "s@https: false@https: true@" \
###  -e "s@# user: git@user: ${gitlabUser}@" \
###  -e "s@# time_zone: 'UTC'@time_zone: Europe/Paris@" \
###  -e "s/email_from: example@example.com/email_from: gitlab@${host}/" \
###  -e "s/email_reply_to: noreply@example.com/email_reply_to: gitlab@${host}/" \
###  config/gitlab.yml
###sudo -u $gitlabUser cp config/secrets.yml.example config/secrets.yml
###sudo -u $gitlabUser sed -i "s@^# db_key_base:@db_key_base: $(apg -m30 -n1)@" config/secrets.yml
###sudo -u $gitlabUser chmod 600 config/secrets.yml
###cp config/secrets.yml /root/${gitlabUser}.secrets.yml
###sudo -u $gitlabUser cp config/database.yml.postgresql config/database.yml
###sudo -u $gitlabUser sed -i -e "s/database: gitlabhq_production/database: ${gitlabUser}/" \
###  -e "s/# username: git/username: ${gitlabUser}/" \
###  -e "s/# password:/password: ${psqlPass}/" config/database.yml
###sudo -u $gitlabUser chmod o-rwx config/database.yml
###sudo -u $gitlabUser chmod 750 /home/${gitlabUser}
###sudo -u $gitlabUser chmod -R u+rwX,go-w log/
###sudo -u $gitlabUser chmod -R u+rwX {tmp/,tmp/pids/,builds,shared/artifacts/}
###sudo -u $gitlabUser chmod -R u+rwX,g+rwX tmp/sockets/
###sudo -u $gitlabUser install -d -m 700 public/uploads/
###sudo -u $gitlabUser cp config/unicorn.rb.example config/unicorn.rb
###sudo -u $gitlabUser sed -i \
###  -e "s@/home/git@/home/${gitlabUser}@g" \
###  -e 's/listen "127.0.0.1:8080", :tcp_nopush => true/#listen "127.0.0.1:8080", :tcp_nopush => true/' \
###  config/unicorn.rb
###sudo -u $gitlabUser cp config/initializers/rack_attack.rb.example config/initializers/rack_attack.rb
###sudo -u $gitlabUser git config --global core.autocrlf input
###sudo -u $gitlabUser git config --global gc.auto 0
###sudo -u $gitlabUser git config --global repack.writeBitmaps true
###sudo -u $gitlabUser cp config/resque.yml.example config/resque.yml
###sudo -u $gitlabUser sed -i "s/redis.sock/${gitlabUser}.sock/" config/resque.yml
###sudo -u $gitlabUser bundle install -j$(nproc) --deployment --without development test mysql aws kerberos
###sudo -u $gitlabUser bundle exec rake gitlab:shell:install REDIS_URL=unix:/var/run/redis/${gitlabUser}.sock RAILS_ENV=production SKIP_STORAGE_VALIDATION=true
###sudo -u $gitlabUser chmod -R ug+rwX,o-rwx /home/${gitlabUser}/repositories/
###sudo -u $gitlabUser chmod -R ug-s /home/${gitlabUser}/repositories/
###sudo -u $gitlabUser chmod g+s /home/${gitlabUser}/repositories/
cd /home/${gitlabUser}
###sudo -u $gitlabUser git clone https://gitlab.com/gitlab-org/gitlab-workhorse.git
cd gitlab-workhorse
###sudo -u $gitlabUser git checkout v$(cat /home/${gitlabUser}/gitlab/GITLAB_WORKHORSE_VERSION)
###sudo -u $gitlabUser make
cd ../gitlab
sudo -u $gitlabUser bundle exec rake gitlab:setup RAILS_ENV=production GITLAB_ROOT_PASSWORD="$GITLAB_ROOT_PASSWORD" GITLAB_ROOT_EMAIL="$GITLAB_ROOT_EMAIL"
sudo -u $gitlabUser sed -i -e "s/app_user=\"git\"/app_user=\"${gitlabUser}\"/" \
  -e "s/# Provides: .*gitlab/# Provides: ${gitlabUser}/" \
  lib/support/init.d/gitlab
sudo -u $gitlabUser sed -i "s@script_path = \"/etc/init.d/gitlab\"@script_path = \"/etc/init.d/${gitlabUser}\"@g" lib/tasks/gitlab/check.rake
sudo -u $gitlabUser git commit -a -m 'change default user'
install -m 755 /home/${gitlabUser}/gitlab/lib/support/init.d/gitlab /etc/init.d/${gitlabUser}
systemctl enable ${gitlabUser}
install -m 644 /home/${gitlabUser}/gitlab/lib/support/logrotate/gitlab /etc/logrotate.d/${gitlabUser}
sed -i "s@/home/git@/home/${gitlabUser}@g" /etc/logrotate.d/${gitlabUser}
adduser www-data $gitlabUser
chmod -R g+rwX /home/${gitlabUser}/gitlab/tmp/{pids,sockets}
install -m 644 /home/${gitlabUser}/gitlab/lib/support/nginx/gitlab-ssl /etc/nginx/sites-available/${gitlabUser}
sed -i -e "s@/home/git@/home/${gitlabUser}@g" \
  -e "s/YOUR_SERVER_FQDN/${host}/g" \
  -e "s@/var/log/nginx/gitlab@/var/log/nginx/${gitlabUser}@g" \
  -e "s/upstream gitlab-workhorse/upstream ${gitlabUser}-workhorse/" \
  -e "s@http://gitlab-workhorse@http://${gitlabUser}-workhorse@" \
  /etc/nginx/sites-available/${gitlabUser}
ln -s /etc/nginx/sites-available/${gitlabUser} /etc/nginx/sites-enabled/
echo "Do the SSL part!"
#/etc/init.d/nginx restart
sudo -u $gitlabUser bundle exec rake assets:precompile RAILS_ENV=production
/etc/init.d/${gitlabUser} start
