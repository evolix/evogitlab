#!/bin/bash

instances="gitlab-demo00 gitlab-nt gitlab-public gitlab-maarch"
for instance in $instances; do
  cd /home/${instance}/gitlab
  sudo -u $instance RAILS_ENV=production bin/background_jobs stop
  sudo -u $instance RAILS_ENV=production bin/background_jobs start
done
