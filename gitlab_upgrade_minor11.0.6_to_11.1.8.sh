#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

gitlabUser="$1"
gitlabVersion="11.1.8"

cd /home/${gitlabUser}/gitlab

## Config file
#echo "Manual merge needed!! Press enter to continue."
#read
#vimdiff config/gitlab.yml <(git show origin/11-0-stable:config/gitlab.yml.example)

# Do a backup
sudo -u $gitlabUser -H bundle exec rake gitlab:backup:create RAILS_ENV=production

# Fetch $gitlabVersion
sudo -u $gitlabUser -H git fetch --all -p
sudo -u $gitlabUser -H git fetch -t
sudo -u $gitlabUser -H git checkout -- Gemfile.lock db/schema.rb yarn.lock locale
sudo -u $gitlabUser -H git checkout -B v${gitlabVersion} tags/v${gitlabVersion}
#sudo -u $gitlabUser -H git pull origin v${gitlabVersion}
sudo -u $gitlabUser -H sed -i -e s'/app_user="git"/app_user="'$gitlabUser'"/' \
  -e 's/# Provides: .*gitlab$/# Provides: '$gitlabUser'/' \
  lib/support/init.d/gitlab
sudo -u $gitlabUser -H sed -i 's#script_path = "/etc/init.d/gitlab"#script_path = "/etc/init.d/'$gitlabUser'"#g' \
  lib/tasks/gitlab/check.rake
install -m 755 /home/${gitlabUser}/gitlab/lib/support/init.d/gitlab /etc/init.d/${gitlabUser}
sudo -u $gitlabUser -H git commit -a --amend -m 'change default user'

# gitlab-shell
cd /home/${gitlabUser}/gitlab-shell
sudo -u $gitlabUser -H git fetch --all
sudo -u $gitlabUser -H git checkout -B v$(</home/${gitlabUser}/gitlab/GITLAB_SHELL_VERSION) \
  tags/v$(</home/${gitlabUser}/gitlab/GITLAB_SHELL_VERSION)
sudo -u $gitlabUser -H bin/compile

# gitlab-workhorse
cd /home/${gitlabUser}/gitlab-workhorse
sudo -u $gitlabUser -H git fetch --all
sudo -u $gitlabUser -H git checkout -B v$(</home/${gitlabUser}/gitlab/GITLAB_WORKHORSE_VERSION) \
  tags/v$(</home/${gitlabUser}/gitlab/GITLAB_WORKHORSE_VERSION)
sudo -u $gitlabUser -H make

# clean and install gems
cd /home/${gitlabUser}/gitlab
sudo -u $gitlabUser -H bundle install --without development test mysql aws kerberos --deployment
sudo -u $gitlabUser -H bundle clean

# gitaly
cd /home/${gitlabUser}/gitaly
pkill -u $gitlabUser gitaly || true
sudo -u $gitlabUser -H git fetch --all --tags
sudo -u $gitlabUser -H git checkout v$(</home/${gitlabUser}/gitlab/GITALY_SERVER_VERSION)
sudo -u $gitlabUser -H cp config.toml{.example,}
sudo -u $gitlabUser -H sed -i "s#/home/git/#/home/${gitlabUser}/#g" config.toml
sudo -u $gitlabUser -H make

# db:migrate, assets, ...
cd /home/${gitlabUser}/gitlab
sudo -u $gitlabUser -H bundle exec rake db:migrate RAILS_ENV=production
# Be sure to have the latest package.json
sudo -u $gitlabUser -H git checkout -- package.json
sudo -u $gitlabUser -H yarn install
sudo -u $gitlabUser -H bundle exec rake yarn:install gitlab:assets:clean gitlab:assets:compile RAILS_ENV=production NODE_ENV=production
sudo -u $gitlabUser -H bundle exec rake gettext:compile RAILS_ENV=production
sudo -u $gitlabUser -H bundle exec rake cache:clear RAILS_ENV=production
sudo -u $gitlabUser -H git commit -a --amend -m "upgraded to $gitlabVersion"

# Restart and check the installation
/etc/init.d/${gitlabUser} restart
sudo -u $gitlabUser -H bundle exec rake gitlab:env:info RAILS_ENV=production
sudo -u $gitlabUser -H bundle exec rake gitlab:check RAILS_ENV=production
cd

mail -s "Mise a jour de GitLab en v${gitlabVersion}" $gitlabUser <<< "Votre GitLab (mutu Evolix) a été mis à jour en v${gitlabVersion}."
