#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

gitlabUser="$1"
gitlabVersion="9.1.4"

cd /home/${gitlabUser}/gitlab
sudo -u $gitlabUser -H bundle exec rake gitlab:backup:create RAILS_ENV=production
sudo -u $gitlabUser -H git fetch --all
sudo -u $gitlabUser -H git checkout -- Gemfile.lock db/schema.rb
sudo -u $gitlabUser -H git checkout -B v${gitlabVersion} tags/v${gitlabVersion}
#sudo -u $gitlabUser -H git pull origin v${gitlabVersion}
sudo -u $gitlabUser -H sed -i -e s'/app_user="git"/app_user="'$gitlabUser'"/' \
  -e 's/# Provides: .*gitlab$/# Provides: '$gitlabUser'/' \
  lib/support/init.d/gitlab
sudo -u $gitlabUser -H sed -i 's#script_path = "/etc/init.d/gitlab"#script_path = "/etc/init.d/'$gitlabUser'"#g' \
  lib/tasks/gitlab/check.rake
install -m 755 /home/${gitlabUser}/gitlab/lib/support/init.d/gitlab /etc/init.d/${gitlabUser}
sudo -u $gitlabUser -H git commit -a --amend -m 'change default user'
cd /home/${gitlabUser}/gitlab-shell
sudo -u $gitlabUser -H git fetch --all
sudo -u $gitlabUser -H git checkout -B v$(cat /home/${gitlabUser}/gitlab/GITLAB_SHELL_VERSION) \
  tags/v$(cat /home/${gitlabUser}/gitlab/GITLAB_SHELL_VERSION)
cd /home/${gitlabUser}/gitlab-workhorse
sudo -u $gitlabUser -H git fetch --all
sudo -u $gitlabUser -H git checkout -B v$(cat /home/${gitlabUser}/gitlab/GITLAB_WORKHORSE_VERSION) \
  tags/v$(cat /home/${gitlabUser}/gitlab/GITLAB_WORKHORSE_VERSION)
sudo -u $gitlabUser -H make
cd /home/${gitlabUser}/gitlab
sudo -u $gitlabUser -H bundle install --without development test mysql aws kerberos --deployment
sudo -u $gitlabUser -H bundle clean
sudo -u $gitlabUser -H bundle exec rake db:migrate RAILS_ENV=production
sudo -u $gitlabUser -H bundle exec rake yarn:install gitlab:assets:clean gitlab:assets:compile RAILS_ENV=production NODE_ENV=production
sudo -u $gitlabUser -H bundle exec rake cache:clear RAILS_ENV=production
sudo -u $gitlabUser -H git commit -a --amend -m "upgraded to $gitlabVersion"
/etc/init.d/${gitlabUser} restart
cd /home/${gitlabUser}/gitlab
sudo -u $gitlabUser -H bundle exec rake gitlab:env:info RAILS_ENV=production
sudo -u $gitlabUser -H bundle exec rake gitlab:check RAILS_ENV=production
cd

mail -s "Mise a jour de GitLab en v${gitlabVersion}" $gitlabUser <<< "Votre GitLab (mutu Evolix) a été mis à jour en v${gitlabVersion}."
